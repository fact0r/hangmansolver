from collections import Counter

def makeDict():
    dict_file = open('dict.txt', 'r')
    svdict = dict_file.read().split("\n")
    return svdict

def wordsOfLength(length, wList):
    return list(filter(lambda x: len(x) == length, wList))

def match (pattern, word):
    p = list(pattern)
    w = list(word)
    for i in range(len(p)):
        if p[i] != w[i] and p[i] != '_':
            return False
    return True

def noWrongLetter(word, letter):
    return word.find(letter) == -1

def allLetters(wList):
    letters = []
    for w in wList:
        letters += list(w)
    return letters

def likelyLetters(letters, pattern):
    for c in pattern:
        letters = list(filter(lambda x: x != c, letters))
    return Counter(letters).most_common(3)

def hangman():
    wordLength = int(input("Wordlenght: "))
    pattern = '_' * wordLength

    words = makeDict()
    words = wordsOfLength(wordLength, words)

    wrongLetters = []

    while(pattern.find('_') != -1):
        index = input("Index of pattern change: ")
        change = input("Letter: ")
        wrongL = input("Wrong Letter: ")

        if change != '' and index != '' and index in "1234567890":
            index = int(index)
            p = list(pattern)
            p[index] = change
            pattern = "".join(p)

        if wrongL != '':
            wrongLetters.append(wrongL)
        else:
            wrongL = " "

        words = list(filter(lambda x:
            noWrongLetter(x, wrongL) and
            match(pattern, x)
            , words
        ))

        letters = allLetters(words)
        lLetters = likelyLetters(letters, pattern)

        if len(words) <= 10:
            print("Possible Words: ", words)
        else:
            print("Words left: ", len(words))
        print("Pattern: ", pattern)
        print("Wrong Letters:", wrongLetters)
        print("Likely letters: ", lLetters)
        print("-"*100)

hangman()
